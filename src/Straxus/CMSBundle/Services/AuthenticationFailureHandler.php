<?php
namespace Straxus\CMSBundle\Services;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AuthenticationFailureHandler extends DefaultAuthenticationFailureHandler
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repo;

    protected $class;

    public function __construct(HttpKernelInterface $httpKernel, EntityManager $em, $class)
    {
        $this->httpKernel = $httpKernel;
        $this->em = $em;
        $this->class = $class;
        $this->repo = $em->getRepository($class);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $r = $request->request;
        $username = $r->get('_username');

        $em = $this->em;
        $user = $em->getRepository("StraxusCMSBundle:User")->findByName($username);
        if ($user != null) {
            $attempt = ($user->getAttempt()) + 1;
            $user->setAttempt($attempt);

            $em->persist($user);
            $em->flush();
        }

        return new RedirectResponse('login');
    }
}