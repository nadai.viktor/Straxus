<?php

namespace Straxus\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EditorController extends Controller
{
    public function indexAction()
    {
        return $this->render('StraxusCMSBundle:Editor:index.html.twig');
    }
}
