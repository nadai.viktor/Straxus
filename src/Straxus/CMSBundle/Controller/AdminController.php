<?php
namespace Straxus\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Straxus\CMSBundle\Entity\User;

class AdminController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('StraxusCMSBundle:User')->findAll();

        return $this->render('StraxusCMSBundle:Admin:index.html.twig', array(
            'entities' => $entities,
        ));
    }
}