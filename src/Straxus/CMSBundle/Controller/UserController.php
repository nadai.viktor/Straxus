<?php

namespace Straxus\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function indexAction()
    {
        return $this->render('StraxusCMSBundle:User:index.html.twig');
    }
}
