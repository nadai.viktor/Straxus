<?php
namespace Straxus\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use \Straxus\CMSBundle\Entity\User;

class SecurityController extends Controller
{

    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');

        $form = $this->createFormBuilder()
            ->add('captcha', 'captcha', array(
                'width' => 200,
                'height' => 50,
                'length' => 6,
            ))->getForm();

        $attempt = 0;

        if ($helper->getLastUsername() != null) {
            $username = $helper->getLastUsername();
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository("StraxusCMSBundle:User")->findByName($username);
            if ($user != null) {
                $attempt = $user->getAttempt();
            }
        }

        return $this->render('StraxusCMSBundle:Security:login.html.twig', array(
            'attempt' => $attempt,
            'form' => $form->createView(),
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ));
    }

    public function loginCheckAction()
    {
        throw new \Exception('This should never be reached!');
    }
}