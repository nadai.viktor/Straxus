<?php

namespace Straxus\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $roles = '';

        switch($user->getRole()) {
            case 'ROLE_ADMIN':
                $roles = 'Adminisztrátor, Felhasználó, Tartalomszerkesztő';
                break;

            case 'ROLE_USEREDITOR':
                $roles = 'Felhasználó, Tartalomszerkesztő';
                break;

            case 'ROLE_USER':
                $roles = 'Felhasználó';
                break;

            case 'ROLE_EDITOR':
                $roles = 'Tartalomszerkesztő';
                break;
        }

        $lastlogin = date_format($user->getLastlogin(), 'Y-m-d H:i:s');

        return $this->render('StraxusCMSBundle:Default:index.html.twig', array(
            'username'  => $user->getUsername(),
            'roles'     => $roles,
            'lastlogin' => $lastlogin,
        ));
    }
}
