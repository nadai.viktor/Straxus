<?php
namespace Straxus\CMSBundle\Listener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

use Doctrine\ORM\EntityManager;
use Straxus\CMSBundle\Entity\User;

class InteractiveLoginListener {

    protected $em;
    protected $request;

    public function __construct(EntityManager $em, Request $request) {

        $this->em = $em;
        $this->request = $request;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event) {

        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof User) {
            if($this->request->hasSession()) {
                $user->setLastlogin(new \DateTime('now'));
                $user->setAttempt(0);
                $this->em->persist($user);
                $this->em->flush();
            }
        }
    }
}