# Straxus Próbafeladat

A feladat megoldásához felhasználtam a GregwarCaptchaBundle alkalmazást:
https://github.com/Gregwar/CaptchaBundle.git

Felhasználók és jelszavak:
* Admin - admin
* User 1 - user
* User 2 - user
* User 3 - user


## Telepítéshez
1. A projekt telepítését composer segítségével lehet elvégezni.
2. Telepítéskor a `straxus` adatbázis nevet kell megadni vagy az sql fájlban kell átírni a nevet.
3. A beimportálható adatbázis mentés `straxus.sql` néven található meg a gyökérkönyvtárban.
4. Az alkalmazás futtatható a `php app/console server:run` parancs kiadásával.

A Symfony project created on September 13, 2015, 9:20 am.